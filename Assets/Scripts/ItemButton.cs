using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 아이템 버튼 컴포넌트
/// </summary>
public class ItemButton : MonoBehaviour
{
    [Header("# 복사할 아이템")]
    public Image m_ItemImage;

    [Header("# 아이템 슬롯")]
    public Image[] m_ItemSlots;

    /// <summary>
    /// 버튼 객체
    /// </summary>
    private Button _ClickedButton;

    private void Awake()
    {
        // Get Button Component
        _ClickedButton = GetComponent<Button>();

        // 버튼 클릭 이벤트 바인딩
        _ClickedButton.onClick.AddListener(CopyItem);
    }

    /// <summary>
    /// 클릭시 아이템 슬롯에 아이템을 복사 생성합니다.
    /// </summary>
    private void CopyItem()
    {
        // 아이템 슬롯 배열 확인
        for(int i = 0; i< m_ItemSlots.Length; ++i)
        {
            // 슬롯의 tag 가 Empty 일때
            if (m_ItemSlots[i].tag == Constants.ITEMSLOT_TAG_EMPTY)
            {
                Image item = Instantiate(m_ItemImage, m_ItemSlots[i].transform);
                item.rectTransform.sizeDelta = new Vector2(100.0f, 100.0f);

                m_ItemSlots[i].tag = Constants.ITEMSLOT_TAG_FILLED;
                break;
            }
        }
    }

}