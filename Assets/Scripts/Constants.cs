

public sealed class Constants
{
    public const string ITEMSLOT_TAG_EMPTY = "Empty";
    public const string ITEMSLOT_TAG_FILLED = "Filled";

    public const string TAG_BUTTON = "Button";
}