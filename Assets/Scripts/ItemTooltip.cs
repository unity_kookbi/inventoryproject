using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 아이템 툴팁 컴포넌트
/// </summary>
public class ItemTooltip : MonoBehaviour
{
    [Header("# 아이템 이미지")]
    public Image m_ItemImage;

    [Header("# 아이템 이름")]
    public TMP_Text m_ItemName;

    [Header("# 아이템 설명")]
    public TMP_Text m_ItemDescription;

    /// <summary>
    /// 화면 너비의 절반값
    /// </summary>
    private float _HalfWidth;

    /// <summary>
    /// RectTransform 입니다.
    /// </summary>
    private RectTransform _rt;

    private void Awake()
    {
        // 시작시 감추기
        gameObject.SetActive(false);
    }

    private void Start()
    {
        // 화면 너비의 절반값
        _HalfWidth = GetComponentInParent<CanvasScaler>().referenceResolution.x * 0.5f;

        // Get RectTransform
        _rt = GetComponent<RectTransform>();
    }

    private void Update()
    {
        // 툴팁의 위치를 마우스 커서 위치로 설정
        transform.position = Input.mousePosition;

        // 툴팁의 x 축 크기와 포지션을 더한 값이 너비 절반보다 크다면
        if (_rt.anchoredPosition.x + _rt.sizeDelta.x > _HalfWidth)
            // 피벗을 툴팁의 우측으로 변경
            _rt.pivot = new Vector2(1, 1);
        else
            _rt.pivot = new Vector2(0, 1);
    }

    /// <summary>
    /// 툴팁의 정보를 초기화
    /// </summary>
    /// <param name="itemName"></param>
    /// <param name="itemDescription"></param>
    /// <param name="itemimage"></param>
    public void SetTooltip(string itemName, string itemDescription, Sprite itemimage)
    {
        m_ItemImage.sprite = itemimage;

        m_ItemName.text = itemName;

        m_ItemDescription.text = itemDescription;
    }

}