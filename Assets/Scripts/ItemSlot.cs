using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// 아이템 슬롯 컴포넌트
/// </summary>
public class ItemSlot : MonoBehaviour, IPointerClickHandler , IDropHandler , IPointerEnterHandler, IPointerExitHandler
{
    [Header("# 아이템 툴팁")]
    public ItemTooltip m_ItemTooltip;

    /// <summary>
    /// 자식으로 있는 아이템 오브젝트 한개를 얻는 메서드
    /// </summary>
    /// <returns>자식 오브젝트</returns>
    private GameObject ItemInSlot()
    {
        if(transform.childCount >0)
            return transform.GetChild(0).gameObject;
        else return null;
    }

    /// <summary>
    /// UI 클릭 이벤트
    /// </summary>
    /// <param name="eventData">마우스 버튼 입력값이 전달됩니다.</param>
    public void OnPointerClick(PointerEventData eventData)
    {
        // 우클릭 시
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            // 태그가 Filled 일때
            if (tag == Constants.ITEMSLOT_TAG_FILLED)
            {
                // 아이템 삭제
                Destroy(ItemInSlot());

                // 태그 Empty 로 변경
                tag = Constants.ITEMSLOT_TAG_EMPTY;
            }
        }
    }

    /// <summary>
    /// UI 드롭 이벤트
    /// </summary>
    /// <param name="eventData"></param>
    public void OnDrop(PointerEventData eventData)
    {
        // 드롭될 슬롯에 아이템이 없을 시 
        if(ItemInSlot() == null)
        {
            // 드래그 된 아이템의 위치와 부모오브젝트를 드롭될 슬롯으로 변경
            Item._BeginDraggedItem.transform.SetParent(transform);
            Item._BeginDraggedItem.transform.position = transform.position;
            
            // 그 슬롯의 태그를 filled 로 변경
            gameObject.tag = Constants.ITEMSLOT_TAG_FILLED;
        }

        // 드롭될 슬롯에 아이템이 존재할 시 스왑!
        else
        {
            // 드롭될 슬롯에 있는 아이템
            GameObject swapItem = ItemInSlot();

            // 드래그 된 아이템의 위치와 부모오브젝트를 드롭될 슬롯으로 변경
            Item._BeginDraggedItem.transform.SetParent(transform);
            Item._BeginDraggedItem.transform.position = transform.position;

            // 드롭될 슬롯에 있는 아이템의 위치와 부모오브젝트를 드래그 했던 아이템의 위치와 부모 오브젝트로 변경
            swapItem.transform.SetParent(Item._StartParent.transform);
            swapItem.transform.position = Item._StartPosition;

            // 스왑한 슬롯들의 태그를 filled 로 변경
            gameObject.tag = Constants.ITEMSLOT_TAG_FILLED;
            Item._StartParent.tag = Constants.ITEMSLOT_TAG_FILLED;

        }
    }

    /// <summary>
    /// 슬롯 위에 마우스가 있는 경우 발생하는 이벤트
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerEnter(PointerEventData eventData)
    {
        // 슬롯에 아이템이 있다면
        if(ItemInSlot() != null)
        {
            // Get Item Component
            Item item = GetComponentInChildren<Item>();

            // 툴팁을 활성화
            m_ItemTooltip.gameObject.SetActive(true);

            // 툴팁을 해당 아이템으로 설정
            m_ItemTooltip.SetTooltip(
                item.itemInfoElement.m_ItemInfo.m_ItemName,
                item.itemInfoElement.m_ItemInfo.m_ItemDescription,
                item.itemInfoElement.m_ItemInfo.m_ItemSprite);
        }
    }

    /// <summary>
    /// 슬롯 위에 마우스가 있다가 나갈 경우 발생하는 이벤트
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerExit(PointerEventData eventData)
    {
        m_ItemTooltip.gameObject.SetActive(false);
    }
}
