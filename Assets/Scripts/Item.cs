using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// 아이템 정보를 갖는 컴포넌트
/// </summary>
public class Item : MonoBehaviour , IBeginDragHandler , IDragHandler , IEndDragHandler
{
    /// <summary>
    /// 드래그 될때 이동되는 아이템을 담을 변수
    /// </summary>
    public static GameObject _BeginDraggedItem;

    [Header("# 아이템 코드")]
    public string m_ItemCode;

    /// <summary>
    /// 드래그 시작시 임시로 부모 오브젝트로 설정할 트랜스폼
    /// 드래그 시작시 UI 레이어에 비정상적으로 보이지 않도록 하기 위해 설정
    /// </summary>
    [Header("# 슬롯들의 부모 오브젝트")]
    public Transform _OnDragParent;

    /// <summary>
    /// 슬롯에 드롭하지 못했을 때 원복할 아이템 위치
    /// </summary>
    public static Vector3 _StartPosition;

    /// <summary>
    /// 슬롯에 드롭하지 못했을 때 원복할 부모 오브젝트
    /// </summary>
    public static Transform _StartParent;

    /// <summary>
    /// 아이템 정보를 담는 ScriptableObject 입니다.
    /// </summary>
    private ItemInfosScriptableObject _ItemInfosScriptableObject;

    /// <summary>
    /// 아이템 정보
    /// </summary>
    private ItemInfoElement _ItemInfoElement;

    /// <summary>
    /// 아이템 정보 파라미터
    /// </summary>
    public ItemInfoElement itemInfoElement => _ItemInfoElement;

    private void Awake()
    {
        // 아이템 정보를 담는 ScriptableObject 에셋 로드
        if ( _ItemInfosScriptableObject == null )
        {
            _ItemInfosScriptableObject = Resources.Load<ItemInfosScriptableObject>("ScriptableObject/ItemInfos");
        }

        // 아이템 정보 초기화
        _ItemInfoElement = _ItemInfosScriptableObject.GetItem(m_ItemCode);

    }

    /// <summary>
    /// 드래그 시작 이벤트
    /// </summary>
    /// <param name="eventData"></param>
    public void OnBeginDrag(PointerEventData eventData)
    {
        // 버튼 안에 있는 이미지에는 작동하지 않게 함
        if (transform.parent.tag == Constants.TAG_BUTTON) return;

        // 드래그 시작 아이템을 설정
        _BeginDraggedItem = gameObject;

        // 원복할 위치 와 부모 오브젝트 설정
        _StartPosition = transform.position;
        _StartParent = transform.parent;

        // 아이템이 없어질 슬롯의 태그 변경
        _StartParent.tag = Constants.ITEMSLOT_TAG_EMPTY;

        // Drop 이벤트를 정상적으로 감지하기 위해 Item RectTransform 을 무시
        GetComponent<CanvasGroup>().blocksRaycasts = false;

        // 드래그 시작시 부모 오브젝트를 변경
        transform.SetParent(_OnDragParent);
    }

    /// <summary>
    /// 드래그 중 이벤트
    /// </summary>
    /// <param name="eventData"></param>
    public void OnDrag(PointerEventData eventData)
    {
        // 버튼 안에 있는 이미지에는 작동하지 않게 함
        if (transform.parent.tag == Constants.TAG_BUTTON) return;

        // 드래그 중 마우스 위치를 적용
        transform.position = eventData.position;
    }

    /// <summary>
    /// 드래그 끝 이벤트
    /// </summary>
    /// <param name="eventData"></param>
    public void OnEndDrag(PointerEventData eventData)
    {
        // 버튼 안에 있는 이미지에는 작동하지 않게 함
        if (transform.parent.tag == Constants.TAG_BUTTON) return;

        // 드래그 대상을 지우고 해당 Item 에 Drop 이벤트 감지를 허용
        _BeginDraggedItem = null;
        GetComponent<CanvasGroup>().blocksRaycasts = true;

        // 슬롯에 드롭하지 못 할 경우 
        if(transform.parent == _OnDragParent)
        {
            // 저장해둔 원래 위치와 부모 오브젝트 하위로
            // 원복
            transform.position = _StartPosition;
            transform.SetParent(_StartParent);

            // 다시 원래 슬롯에 채워지므로 태그 다시 변경
            _StartParent.tag = Constants.ITEMSLOT_TAG_FILLED;
        }
    }



}